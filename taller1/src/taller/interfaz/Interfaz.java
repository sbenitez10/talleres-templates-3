package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;
	
	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;
	
	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */

public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
    }
	
	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{ 
		try
		{
		  Scanner sc = new Scanner(System.in);
		  System.out.println("-----------------Configuracion del juego ------------------");
		  System.out.println("Numero de filas: ");
		  int Fila = sc.nextInt();
		  boolean error = true;
		   while (error== true)
		   {
			  if (Fila < 4)
			  {
				  System.out.println("Numero de filas debe ser mayor a 3");
				  System.out.println("Numero de filas: ");
				  Fila = sc.nextInt();
			  }
			  else
			   {
			    error = false;
			   }
		   }
			 
		  System.out.println("Numero de columnas: ");
		  int Col = sc.nextInt();
		  error = true;
		   while (error== true)
		   {
			  if (Col < 4)
			  {
				  System.out.println("Numero de Columnas debe ser mayor a 3");
				  System.out.println("Numero de Columnas: ");
				  Col= sc.nextInt();
			  }
			  else
			   {
			    error = false;
			   }
		   }
			  System.out.println("Numero de Jugadores: ");
			  int jug = sc.nextInt();
			  ArrayList<Jugador> players = new ArrayList<Jugador>();
			 int c =0;
			 
			  for (int d=0; d<jug;d++)
			  {
				  String nombre = null;
				  String simbolo =null;
				  
				  System.out.println("Nombre del jugador" +d +":");
				   nombre = sc.nextLine();
				
				  
			
				  System.out.println("Simbolo del jugador" + d +":");
				   simbolo =sc.nextLine();
				   error = true;
				   while (error== true)
				   {
					   if (simbolo.length() != 1)
						  {
							  System.out.println("su Simbolo solo puede ser de un caracter:");
							  System.out.println("Simbolo del jugador:");
							  simbolo= sc.nextLine();
							  
						  }
					   else
					   {
					    error = false;
					   }
				   }
	
				  Jugador a = new Jugador(nombre, simbolo);
				  
				  players.add(a);
			  }
			  LineaCuatro juegoamigos = new LineaCuatro(players, Fila, Col);
			  juego(juegoamigos);
		
		}
		  catch (Exception e)        
		  {            
			  System.err.println("Error:" + e.getMessage());                         e.printStackTrace(); 
	      }
		
		
       //TODO
	}
	
	/**
	 * Modera el juego entre jugadores
	 */
	public void juego(LineaCuatro pJuego)
	{	int con =0;
		boolean termino = false;
		while(termino == false)
		{
			Scanner sc = new Scanner(System.in);
			String juega= pJuego.darAtacante();
			
			  System.out.println("juega:" + juega);
			  System.out.println("coloca el numero de la columna que quieres poner tu ficha:");
			  int col = sc.nextInt();
			  boolean error= true;
					  while(error== true)
					  {
							  if (col>= pJuego.darTablero()[0].length)
							  {
								  System.out.println("tiene que ser un numero entre"+" " +0+ " " +"y"+" "+ (pJuego.darTablero()[0].length-1) +":");
								  col = sc.nextInt();
							  }
							  else
							  {
								  error= false;
							  }
					  }
			  pJuego.registrarJugada(col);
			  imprimirTablero(pJuego);
			  for (int i=0; i<pJuego.darTablero().length ;i++)
				{
				for (int j=0; j< pJuego.darTablero()[0].length; j++)
					{  
					      if( pJuego.terminar(i, j) == true)
					      {
					    	  con++;
					      }
					}
				}
			  if (con != 0)
				 {
					 termino = true;
					 System.out.println("ganador:" + juega);
				 }
		}
			
		
		
		//TODO
	}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		try
		{
		  Scanner sc = new Scanner(System.in);
		  System.out.println("-----------------Configuracion del juego ------------------");
		  System.out.println("Numero de filas: ");
		  int Fila = sc.nextInt();
		  boolean error = true;
		   while (error== true)
		   {
			  if (Fila < 4)
			  {
				  System.out.println("Numero de filas debe ser mayor a 3");
				  System.out.println("Numero de filas: ");
				  Fila = sc.nextInt();
			  }
			  else
			   {
			    error = false;
			   }
		   }
			 
		  System.out.println("Numero de columnas: ");
		  int Col = sc.nextInt();
		  error = true;
		   while (error== true)
		   {
			  if (Col < 4)
			  {
				  System.out.println("Numero de Columnas debe ser mayor a 3");
				  System.out.println("Numero de Columnas: ");
				  Col= sc.nextInt();
			  }
			  else
			   {
			    error = false;
			   }
		   }
			  
			  
			  ArrayList<Jugador> players = new ArrayList<Jugador>();
			 
			  String nombre = null;
			  String simbolo =null;				  
				
				  System.out.println("Nombre del jugador:");
				   nombre = sc.nextLine();
				
				  System.out.println("Simbolo del jugador:");
				   simbolo =sc.nextLine();
				   error = true;
				   while (error== true)
				   {
					   if (simbolo.length() != 1)
						  {
							  System.out.println("su Simbolo solo puede ser de un caracter:");
							  System.out.println("Simbolo del jugador:");
							  simbolo= sc.nextLine();
							  
						  }
					   else
					   {
					    error = false;
					   }
				   }
					  
				  
					  Jugador a = new Jugador(nombre, simbolo);
					  String maquina = "maquina";
					  String simboloma = "X";
					  Jugador maq = new Jugador( maquina, simboloma);
				  
				  players.add(a);
				  players.add(maq);
				  
			  
			  LineaCuatro juegoamigos = new LineaCuatro(players, Fila, Col);
			  imprimirTablero(juegoamigos);
			  juegoMaquina(juegoamigos);
		
		}
		  catch (Exception e)        
		  {            
			  System.err.println("Error:" + e.getMessage());                         e.printStackTrace(); 
	      }
		
		
       
	}
		
	
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina( LineaCuatro pJuego)
	{   int con=0;
		boolean termino = false;
		Scanner sc = new Scanner(System.in);
		while(termino == false)
		{
			
			String juega= pJuego.darAtacante();
			if(juega.equals("maquina") == false)
			{
			  System.out.println("juega:" + juega);
			  System.out.println("coloca el numero de la columna que quieres poner tu ficha:");
			  int col = sc.nextInt();
			  boolean error= true;
			  while(error== true)
			  {
					  if (col>= pJuego.darTablero()[0].length)
					  {
						  System.out.println("tiene que ser un numero entre"+" " +0+ " " +"y"+" "+ (pJuego.darTablero()[0].length-1) +":");
						  col = sc.nextInt();
					  }
					  else
					  {
						  error= false;
					  }
			  }
			  pJuego.registrarJugada(col);
			  imprimirTablero(pJuego);
			  for (int i=0; i<pJuego.darTablero().length ;i++)
				{
				for (int j=0; j< pJuego.darTablero()[0].length; j++)
					{  
					      if( pJuego.terminar(i, j) == true)
					      {
					    	  con++;
					      }
					}
				}
			}
			if (con != 0)
			 {
				 termino = true;
				 System.out.println("ganador:" + juega);
			 }
			if(juega.equalsIgnoreCase("maquina")== true)
			{
				 System.out.println("juega:" + juega);
				 pJuego.registrarJugadaAleatoria();
				 imprimirTablero(pJuego);
				 for (int i=0; i<pJuego.darTablero().length ;i++)
					{
					for (int j=0; j< pJuego.darTablero()[0].length; j++)
						{  
						      if( pJuego.terminar(i, j) == true)
						      {
						    	  con++;
						      }
						}
					}
				 if (con != 0)
				 {
					 termino = true;
					 System.out.println("Ganador:" + juega);
				 }
			}
			
		}
		  
		
		//TODO
	}

	/**
	 * Imprime el estado actual del juego
	 * @param pjuego 
	 */
	public void imprimirTablero(LineaCuatro pjuego)
	{
		String [][] tabla = pjuego.darTablero();
		
		for (int i=0; i<tabla.length ;i++)
			{
			for (int j=0; j< tabla[0].length; j++)
				{
				System.out.print(tabla[i][j] + "\t" );
				}
			System.out.println();
			}
		//TODO
	}
}
